package utils

import "time"

func GetLocalTime() time.Time {
	return time.Now()
}
